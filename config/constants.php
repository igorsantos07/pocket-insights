<?php
define('ROOT',      dirname(__DIR__).'/');
define('LOG_DIR',   ROOT.'logs/');
define('LESS_DIR',  ROOT.'www/assets/less/');
define('CSS_DIR',   ROOT.'www/assets/');
define('LOG_FILE',  LOG_DIR.'main.log');

$env = getenv('POCKET_ENVIRONMENT');
define('ENV', $env ? : (isset($_SERVER['HTTP_HOST']) ? ($_SERVER['HTTP_HOST'] != 'pocket.igoru.dev') : 'dev'));
define('PROD', ENV == 'prod');

define('SESSION_DURATION', 60*60*24*365);
define('SESSION_DIR', sys_get_temp_dir().DIRECTORY_SEPARATOR.'pocket-sessions');

define('CONSUMER_KEY', '15132-b7f3ec93ed64870669b833a5');