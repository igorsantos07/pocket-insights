<?php
ini_set('session.gc_maxlifetime', SESSION_DURATION);
if (!file_exists(SESSION_DIR)) mkdir(SESSION_DIR, 0777);
session_save_path(SESSION_DIR);
session_set_cookie_params(SESSION_DURATION);
session_start();
