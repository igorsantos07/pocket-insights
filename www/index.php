<?php
/* ***************************************** DEFINING CONSTANTS AND AUTOLOAD **************************************** */
require_once '../config/constants.php';
require_once ROOT.'vendor/autoload.php';


/* ******************************************** CONFIGURING THE SILEX APP ******************************************* */

/**
 * Exception used to show errors to the user
 */
class UserException extends Exception {}

use \Silex\Application;
class App extends Application {
	use Application\UrlGeneratorTrait;
	use Application\MonologTrait;
	use Application\TwigTrait;
}
$app = new App();
$app['debug'] = !PROD;

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app->register(new Silex\Provider\TwigServiceProvider(), ['twig.path' => ROOT.'views']);

$app->register(new Silex\Provider\MonologServiceProvider(), ['monolog.logfile' => ROOT.'logs/main.log']);

$app->register(new \Guzzle\GuzzleServiceProvider(), ['guzzle.base_url' => 'https://getpocket.com/v3/']);
require_once ROOT.'lib/PocketAPI.php';
PocketAPI::$guzzle = $app['guzzle.client'];


/* **************************************** REQUIRING CONTROLLERS AND RUNNING *************************************** */

foreach (scandir(ROOT.'controllers') as $controller) {
	if (preg_match('/.*.php$/', $controller))
		require_once ROOT."controllers/$controller";
}

require_once '../config/session.php';

$app->run();
