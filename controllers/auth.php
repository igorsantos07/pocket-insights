<?php

$app->before(function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
	$request->isAuth = isset($_SESSION['username']);
});

$app->get('/login', function () use ($app) {
	$redirect_to  = $app->url('authorized');
	$redirect_uri = urlencode($redirect_to);

	$response = PocketAPI::request('oauth/request', ['redirect_uri' => $redirect_to]);
	$code     = $response->getStatusCode();

	if ($code != 200) {
		$error     = sprintf('[%d] %s', $response->getHeader('X-Error-Code'), $response->getHeader('X-Error'));
		$exception = ($code >= 500) ? 'UserException' : 'Exception';
		throw new $exception($error);
	}
	else {
		$_SESSION['auth_code'] = json_decode($response->getBody(), true)['code'];;

		return $app->redirect("https://getpocket.com/auth/authorize?request_token={$_SESSION['auth_code']}&redirect_uri=$redirect_uri");
	}
})
->bind('login');

$app->get('/authorized', function () use ($app) {
	$response = PocketAPI::request('oauth/authorize', ['code' => $_SESSION['auth_code']]);
	$code     = $response->getStatusCode();

	if ($code != 200) {
		$pocket_code = $response->getHeader('X-Error-Code');
		if ($code == 403 && $pocket_code == 158) {
			$error     = 'You rejected the Pocket authorization process... Please, try again!';
			$exception = 'UserException';
		}
		else {
			$error     = sprintf('[%d] %s', $pocket_code, $response->getHeader('X-Error'));
			$exception = ($code >= 500) ? 'UserException' : 'Exception';
		}
		throw new $exception($error);
	}
	else {
		$data = (json_decode($response->getBody(), true));

		unset($_SESSION['auth_code']);
		$_SESSION['access_token']     = $data['access_token'];
		$_SESSION['username']         = $data['username'];

		return $app->redirect($app->url('home'));
	}

})
->bind('authorized');