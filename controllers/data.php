<?php
require_once '../lib/Date.php';

$app->get('/', function () use ($app) {
	if (isset($_GET['debug']))
		$debug = !empty($_GET['debug']) ? $_GET['debug'] : true;
	else
		$debug = false;

	$username = $app['request']->isAuth ? $_SESSION['username'] : null;
	$raw_data = '';

	if ($app['request']->isAuth) {
		$data = PocketAPI::getRequestData('get', ['state' => 'all', 'sort' => 'oldest'])['list'];

		$remove = [];
		foreach ($data as $id => $item) {
			if ($item['status'] == PocketAPI::STATUS_DELETED) {
				$remove[] = $id;
				continue;
			}

			if ($debug) {
				if ($debug == 'hard') {
					$stamp = \Helper\Date::timestampNoTime($item['time_added']);
					$raw_data .= sprintf('[%s] %s (%s [%s]) | %s %s',
						$item['status'] ? 'V' : 'X',
						date('d/M/y H:i', $item['time_added']),
						$stamp,
						$item['time_added'],
						str_pad(($item['time_read'] ? date('d/M/Y', $item['time_read']) : 'X'), 12),
						$item['given_title']
					)."\n";
				}
				else {
					$raw_data .= sprintf('[%s] %s | %s %s',
							$item['status'] ? 'V' : 'X',
							date('d/M/y H:i', $item['time_added']),
							str_pad(($item['time_read'] ? date('d/M/Y', $item['time_read']) : 'X'), 12),
							$item['given_title']
						)."\n";
				}
			}
		}

		foreach ($remove as $id)
			unset($data[$id]);

		$sums = array_reduce($data, function ($result, $item) {
			$statuses = ($item['status'] == PocketAPI::STATUS_READ) ?
				[PocketAPI::STATUS_UNREAD, PocketAPI::STATUS_READ] : [PocketAPI::STATUS_UNREAD];

			foreach ($statuses as $status) {
				$read       = $status == PocketAPI::STATUS_READ;
				$not_status = $read ? PocketAPI::STATUS_UNREAD : PocketAPI::STATUS_READ;

				$time  = $read ? $item['time_read'] : $item['time_added'];
				$stamp = \Helper\Date::timestampNoTime($time);

				if (array_key_exists($stamp, $result))
					++$result[$stamp][$status];
				else
					$result[$stamp] = [$status => 1, $not_status => 0];
			}

			return $result;
		}, []);

		ksort($sums);

		$org_sums = $unread = [];
		$cumulative = 0;
		foreach ($sums as $date => $totals) {
			$org_sums[] = $details = [
				'date'    => $date,
				'read'    => $totals[PocketAPI::STATUS_READ],
				'unread'  => $totals[PocketAPI::STATUS_UNREAD],
				'balance' => $totals[PocketAPI::STATUS_UNREAD] - $totals[PocketAPI::STATUS_READ]
			];

			$unread[] = [
				'date' => $date,
				'unread' => $cumulative = $cumulative + $details['balance']
			];
		}
	}

	return $app->render('index.twig', compact('username', 'debug', 'sums', 'org_sums', 'unread', 'raw_data'));
})
->bind('home');
