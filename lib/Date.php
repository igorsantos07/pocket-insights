<?php
namespace Helper;

class Date {

	public static function timestampNoTime($ts) {
//		$date = \DateTime::createFromFormat('U', $ts, new \DateTimeZone(date_default_timezone_get()));
//		$date->setTime(0,0,0);
//		return $date->getTimestamp();

		return date('U', strtotime(date('M d, Y', $ts)));
	}

}