<?php


class PocketAPI {

	const STATUS_UNREAD  = 0;
	const STATUS_READ    = 1;
	const STATUS_DELETED = 2;

	/**
	 * @var \Guzzle\Http\Client
	 */
	public static $guzzle;

	public static function request($action, array $fields = []) {
		$default_fields = ['consumer_key' => CONSUMER_KEY];
		if (isset($_SESSION['access_token'])) $default_fields['access_token'] = $_SESSION['access_token'];

		/** @var \Guzzle\Http\Message\RequestInterface $request */
		$request = self::$guzzle->post($action);
		$request->addHeader('Content-Type', 'application/x-www-form-encoded; charset=UTF8')
		->addHeader('X-Accept', 'application/json')
		->addPostFields(array_merge($fields, $default_fields));

		return $request->send();
	}

	public static function getRequestData($action, array $fields = [], $asObject = false) {
		$request = self::request($action, $fields);
		return json_decode($request->getBody(), !$asObject);
	}

}