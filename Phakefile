<?php
require_once 'config/constants.php';
require_once 'vendor/autoload.php';

function run($command, $success, $failure) {
	if ($command()) {
		echo $success."\n";
	}
	else {
		echo $failure.". Aborting!\n";
		exit;
	}
}

desc('Prepares files and folders correctly');
task('prepare', function () {
	foreach ([LOG_DIR, LESS_DIR, CSS_DIR] as $folder) {
		if (!file_exists($folder))
			run(function () use ($folder) {
				return mkdir($folder);
			}, 'One folder created: '.$folder, 'Error creating folder: '.$folder);

		run(function () use ($folder) {
			return chmod($folder, 0777);
		}, "Folder with permissions: $folder", "Error giving 0777 to folder: $folder");
	}

	if (file_exists('logs/main.log'))
		run(function () {
			return rename(LOG_FILE, LOG_FILE.'.old');
		}, 'Old log file moved', 'Failed to move old log file');

	run(function () {
		return file_put_contents(LOG_FILE, '') !== false;
	}, 'Log file created', 'Error creating log file');

	run(function () {
		return chmod(LOG_FILE, 0777);
	}, 'Log file with permissions', 'Error giving 0777 to log file');
});

group('compile', function () {
	function add_files_to_list($dir, &$files) {
		foreach (scandir($dir) as $basename) {
			if ($basename == '.' || $basename == '..') continue;

			$path = $dir.DIRECTORY_SEPARATOR.$basename;

			if (is_dir($path))
				add_files_to_list($path, $files);
			elseif (substr($basename, -5) == '.less')
				$files[] = ['path' => $path, 'mtime' => filemtime($path)];
		}
	}

	$less_files = function() {
		$files = [];
		add_files_to_list(LESS_DIR, $files);
		return $files;
	};

	$compile_less = function () use ($less_files) {
		$tmp_name = tempnam('/tmp', 'pocket_less');
		$tmp_file = fopen($tmp_name, 'w');
		$files = 0;
		foreach ($less_files() as $file) {
			$less = $file['path'];
			if (strpos($less, 'inc/') !== false) continue;

			run(function () use ($tmp_file, $less, &$files) {
				$result = fwrite($tmp_file, file_get_contents($less));
				if ($result !== false) {
					++$files;
					return true;
				}
			}, '+ '.basename($less), 'Failed to add to temp LESS file: '.basename($less));
		}

		echo "$files file".($files == 1? '' : 's')." to compile. ";
		if ($files == 0) {
			echo "No need to compile.\n";
		}
		else {
			echo "Compiling...\n";

			try {
				$css_file = CSS_DIR.'styles.css';
				touch($css_file);

				$lessc = new lessc;
				$lessc->setFormatter('compressed');
				$lessc->setImportDir([
					LESS_DIR,
					ROOT.'vendor/',
					ROOT.'www/assets/',
					//added this special dir because its ridiculous big path
					ROOT.'vendor/laughingwithu/flawless-semantics-grid/project/stylesheets/',
				]);
				$lessc->compileFile($tmp_name, $css_file);

				echo "LESS compiled to $css_file.\n";
				unlink($tmp_name);
			}
			catch (Exception $e) {
				echo 'Oops! Error compiling LESS: '.$e->getMessage()."\n";
			}
		}
	};

	desc('Compiles all LESS files into one CSS file');
	task('less', $compile_less);

	//TODO: Could show the changed files when notices it needs recompiling
	desc('Verifies LESS files each 3 seconds (default=3); if changed, recompiles. Args: time=int, force=bool.');
	task('watch', function ($args) use ($less_files, $compile_less) {
		$should_clean = false;
		while(true) {
			echo ($should_clean)? "\r\e[K" : "\n";

			$files_log = $less_files();
			$compiled_log = LOG_DIR.'compiled_less.list';
			if ((isset($args['force']) && $args['force']) || !file_exists($compiled_log)) {
				$compile = true;
				unset($args['force']);
			}
			else {
				$log = unserialize(file_get_contents($compiled_log));
				$compile = ($log != $files_log);
			}

			if ($compile) {
				file_put_contents($compiled_log, serialize($files_log));
				echo "Recompiling... \n";
				$compile_less();
				echo "\n";
			}
			else {
				echo "Nothing to do here! ".date('H:i:s');
				$should_clean = true;
			}

			sleep((isset($args['time'])? $args['time'] : 3));
		}
	});

});

desc('Installs dependencies and should probably do more stuff later');
task('build', function () {
	passthru('composer update');
});

desc('Installs everything the system depends on, including libraries and folders and stuff');
task('install', 'prepare', 'build', 'compile:less');